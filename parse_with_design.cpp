#include <panel.h>
#include <cstring>
#include <string>
#include <vector>
#include <fstream>

#define NLINES 20
#define NCOLS 30

void init_wins(WINDOW **wins);
void win_show(WINDOW *win, char *label, int label_color);
void print_params(WINDOW *win, int starty, int startx, int width, char *string, chtype color);


std::vector<std::string> parse_text(char* symb) {
    std::vector<std::string> res_text;
    std::string str = "";

    char* curr_pos = symb;
    while(*curr_pos != '\0') {
    	if (!isspace(*curr_pos)) {
    		str += *curr_pos;
    	} else {
            res_text.push_back(str);
            str = "";
        }
        curr_pos++;
    }
    if (str.size()) {
        res_text.push_back(str);
    }
    return res_text;
}

int main()
{	WINDOW *my_wins[2];
	PANEL  *my_panels[2];
	PANEL  *top;
	int ch;

	/* Initialize curses */
	initscr();
	start_color();
	cbreak();
	raw(); // no buffering
	keypad(stdscr, TRUE);

	/* Initialize all the colors */
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_BLUE, COLOR_BLACK);
	init_pair(4, COLOR_MAGENTA, COLOR_BLACK);

	init_wins(my_wins);
	
	/* Attach a panel to each window */ 	/* Order is bottom up */
	my_panels[0] = new_panel(my_wins[0]); 	/* Push 0, order: stdscr-0 */
	my_panels[1] = new_panel(my_wins[1]); 	/* Push 1, order: stdscr-0-1 */

	/* Set up the user pointers to the next panel */
	set_panel_userptr(my_panels[0], my_panels[1]);
	set_panel_userptr(my_panels[1], my_panels[0]);


	/* Update the stacking order. 1 panel will be on top */

	/* Show it on the screen */
	attron(COLOR_PAIR(4));
	printw("Use tab or enter to browse through the windows (F2 to Exit)");
	attroff(COLOR_PAIR(1));
	refresh();
	endwin();

	
	// doupdate();

	std::vector<std::string> res_str;
	while((ch = getch()) != KEY_F(2)) {
		top_panel(my_panels[1]);
		update_panels();
	    doupdate();

		char buf[4096];
    	std::ofstream out;
    	out.open("in.txt", std::ios::out);
    	mvwgetstr(my_wins[0], 5, 5, buf);
				
    	res_str = parse_text(buf);
    	for (int i = 0; i < res_str.size(); ++i) {
        	mvwprintw(my_wins[1], 5 + i, 5, res_str[i].c_str()); 
        	out << res_str[i] << "\n"; 
        }
        update_panels();
        doupdate();
	}
    endwin();
	return 0;
}

/* Put all the windows */
void init_wins(WINDOW **wins)
{	
	int x, y;
	char label[80];

	y = 2;
	x = 5;
	wins[0] = newwin(NLINES, NCOLS, y, x);
	sprintf(label, "Input text");
	win_show(wins[0], label, 3);
	wins[1] = newwin(NLINES, NCOLS, y, x + 40);
	sprintf(label, "Parsed text");
	win_show(wins[1], label, 3);
}

/* Show the window with a border and a label */
void win_show(WINDOW *win, char *label, int label_color)
{	int startx, starty, height, width;

	getbegyx(win, starty, startx);
	getmaxyx(win, height, width);

	box(win, 0, 0);
	mvwaddch(win, 2, 0, ACS_LTEE); 
	mvwhline(win, 2, 1, ACS_HLINE, width - 2); 
	mvwaddch(win, 2, width - 1, ACS_RTEE); 
	
	print_params(win, 1, 0, width, label, COLOR_PAIR(label_color));
}

void print_params(WINDOW *win, int starty, int startx, int width, char *string, chtype color)
{	int length, x, y;
	float temp;

	if(win == NULL)
		win = stdscr;
	getyx(win, y, x);
	if(startx != 0)
		x = startx;
	if(starty != 0)
		y = starty;
	if(width == 0)
		width = 80;

	length = strlen(string);
	temp = (width - length)/ 2;
	x = startx + (int)temp;
	wattron(win, color);
	mvwprintw(win, y, x, "%s", string);
	wattroff(win, color);
	refresh();
}


