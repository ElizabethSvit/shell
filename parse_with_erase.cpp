#include <panel.h>
#include <cstring>
#include <string>
#include <vector>
#include <fstream>

#define NLINES 20
#define NCOLS 30

WINDOW* init_wins(WINDOW **win, PANEL **panel);

std::vector<std::string> parse_text(char* symb) {
    std::vector<std::string> res_text;
    std::string str = "";

    char* curr_pos = symb;
    while(*curr_pos != '\0') {
    	if (!isspace(*curr_pos)) {
    		str += *curr_pos;
    	} else {
            res_text.push_back(str);
            str = "";
        }
        curr_pos++;
    }
    if (str.size()) {
        res_text.push_back(str);
    }
    return res_text;
}

int main()
{	WINDOW *my_wins[2];
	PANEL  *my_panels[2];
	PANEL  *top;
	int ch;

	/* Initialize curses */
	initscr();
	start_color();
	cbreak();
	raw(); // no buffering
	keypad(stdscr, TRUE);

	/* Initialize all the colors */
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_BLUE, COLOR_BLACK);
	init_pair(4, COLOR_MAGENTA, COLOR_BLACK);

	init_wins(my_wins, my_panels);

	/* Show it on the screen */
	attron(COLOR_PAIR(4));
	printw("Use tab or enter to start typing (F2 to Exit)");
	refresh();
	endwin();

	std::vector<std::string> res_str;
	while((ch = getch()) != KEY_F(2)) {
		if (ch == KEY_ENTER || ch == '\t') { // not working
			werase(my_wins[1]);
			init_wins(my_wins, my_panels);
		}
		top_panel(my_panels[1]);

		update_panels();
	    doupdate();

		char buf[4096];
    	std::ofstream out;
    	out.open("in.txt", std::ios::out);
    	mvwgetstr(my_wins[0], 5, 5, buf);
				
    	res_str = parse_text(buf);
    	for (int i = 0; i < res_str.size(); ++i) {
        	mvwprintw(my_wins[1], 5 + i, 5, res_str[i].c_str()); 
        	out << res_str[i] << "\n"; 
        }

        my_wins[0] = newwin(NLINES, NCOLS, 2, 5);
		mvwprintw(my_wins[0], 2, 2, "%s", "Input text\n");
		box(my_wins[0], 0, 0);
		my_panels[0] = new_panel(my_wins[0]); 	/* Push 0, order: stdscr-0 */
		set_panel_userptr(my_panels[0], my_panels[1]);
        
        update_panels();
        doupdate();
	}
    endwin();
	return 0;
}

WINDOW* init_wins(WINDOW **my_wins, PANEL **my_panels) {
	int y = 2;
	int x = 5;
	char label[80];
	my_wins[0] = newwin(NLINES, NCOLS, y, x);
	mvwprintw(my_wins[0], 2, 2, "%s", "Input text\n");

	my_wins[1] = newwin(NLINES, NCOLS, y, x + 40);
	mvwprintw(my_wins[1], 2, 2, "%s", "Parsed text\n");


    box(my_wins[0], 0, 0);
    box(my_wins[1], 0, 0);
	
	/* Attach a panel to each window */ 	/* Order is bottom up */
	my_panels[0] = new_panel(my_wins[0]); 	/* Push 0, order: stdscr-0 */
	my_panels[1] = new_panel(my_wins[1]); 	/* Push 1, order: stdscr-0-1 */

	/* Set up the user pointers to the next panel */
	set_panel_userptr(my_panels[0], my_panels[1]);
	set_panel_userptr(my_panels[1], my_panels[0]);
}

